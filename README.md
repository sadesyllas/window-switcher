# Window Switcher

A program written in Rust, using [gtk-rs](https://gtk-rs.org/) and
[ngrammatic](https://github.com/compenguy/ngrammatic), to provide an
alternative way to navigate through the currently open windows.

## Dependencies

To compile and use this program, all Gtk dependencies, according to
the documentation at [gtk-rs](https://gtk-rs.org/) must be present
for the target platform.

This program assumes that the [wmctrl](http://tripie.sweb.cz/utils/wmctrl/)
command is present on the target platform.

## Usage

```shell
$ window-switcher -h
Window Switcher 1.0.0

USAGE:
    window-switcher [FLAGS] [OPTIONS]

FLAGS:
    -w, --current-workspace    Restricts the window list to the current workspace.
    -h, --help                 Prints help information
    -V, --version              Prints version information

OPTIONS:
    -c, --css-file <FILE>    Chooses a CSS file to use for styling the application's window.
    -b, --rgba <RGBA>        Chooses the background for the application's window (e.g., 38.0,50.0,56.0,1.0).
```

- Run the `window-switcher` binary to bring up a window showing
  all of the currently open windows.
- Type some text that should match either the title of the desired
  window or the command that created that window.
- When the desired result is the first one, then press the `Enter` key
  to select it.
- Alternatively, use the arrow keys to navigate through the window
  list and select the desired window by pressing the `Enter` key.
- The `Escape` key will quit the program.
- When navigating through the window list, typing will focus the
  search input box.
