use crate::window_info::WindowInfo;
use crate::Opts;
use regex::Regex;
use std::collections::{HashMap, HashSet};
use std::fs;
use std::path::{self, Path};
use std::process::Command;

pub fn build_window_infos(opts: &Opts) -> Vec<WindowInfo> {
    let whitespace_re = Regex::new(r"\s+").unwrap();

    let current_workspace = {
        if opts.current_workspace_only {
            get_current_workspace_string()
        } else {
            String::new()
        }
    };

    let mut active_window_id = get_active_window_id();
    active_window_id.replace_range(0..2, "0x0");

    let mut window_infos_hashset: HashSet<String> = HashSet::new();

    let mut window_infos: Vec<WindowInfo> = get_window_infos_string()
        .lines()
        .filter_map(|line| {
            let line_parts: Vec<&str> = whitespace_re.splitn(line, 5).collect();

            let (id, workspace, pid, title) = (
                line_parts[0].to_owned(),
                line_parts[1],
                line_parts[2],
                line_parts[4].to_owned(),
            );

            if id.eq(active_window_id.as_str()) || workspace == "-1" || title.is_empty() {
                return None;
            }

            if opts.current_workspace_only && workspace != current_workspace {
                return None;
            }

            let root = path::MAIN_SEPARATOR.to_string();
            let proc_command_path = Path::new(root.as_str()).join("proc").join(pid).join("comm");

            let command = fs::read(proc_command_path).unwrap();
            let command = String::from_utf8(command).unwrap().trim().to_owned();

            window_infos_hashset.insert(id.clone());

            Some(WindowInfo::new(id, title, command))
        })
        .collect();

    let window_id_re = Regex::new(r"^0x[0-9a-f]+\s.+").unwrap();

    let mut root_window_tree_indexes: HashMap<String, usize> = HashMap::new();

    get_root_window_tree_string()
        .lines()
        .filter_map(|line| {
            let line = line.trim_start();

            if !window_id_re.is_match(line) {
                return None;
            }

            let mut window_id = whitespace_re
                .splitn(line, 2)
                .collect::<Vec<_>>()
                .first()
                .unwrap()
                .to_string();

            // wmctrl returns 0x0... whereas xwininfo returns 0x...
            window_id.replace_range(0..2, "0x0");

            if !window_infos_hashset.contains(window_id.as_str()) {
                return None;
            }

            Some(window_id)
        })
        .enumerate()
        .for_each(|(index, window_id)| {
            root_window_tree_indexes.insert(window_id, index);
        });

    window_infos.sort_by(|a, b| {
        root_window_tree_indexes[a.id()]
            .partial_cmp(&root_window_tree_indexes[b.id()])
            .unwrap()
    });

    window_infos
}

pub fn switch_to_window_by_id(id: &str) {
    let args = &["-i", "-a", id];
    Command::new("wmctrl").args(args).output().unwrap();
}

fn get_window_infos_string() -> String {
    let command = Command::new("wmctrl").arg("-dlp").output().unwrap();

    String::from_utf8(command.stdout).unwrap()
}

fn get_root_window_tree_string() -> String {
    let args = &["-root", "-tree"];
    let command = Command::new("xwininfo").args(args).output().unwrap();

    String::from_utf8(command.stdout).unwrap()
}

fn get_current_workspace_string() -> String {
    let result = get_workspaces_string();

    let whitespace_re = Regex::new(r"\s+").unwrap();

    result
        .lines()
        .filter_map(|line| {
            let line_parts: Vec<&str> = whitespace_re.splitn(line, 3).collect();
            let (id, symbol) = (line_parts[0], line_parts[1]);

            if symbol == "*" {
                return Some(id);
            }

            None
        })
        .collect::<Vec<_>>()
        .into_iter()
        .next()
        .unwrap_or("")
        .to_owned()
}

fn get_workspaces_string() -> String {
    let command = Command::new("wmctrl").arg("-d").output().unwrap();

    String::from_utf8(command.stdout).unwrap()
}

fn get_active_window_id() -> String {
    let args = &["-root", "_NET_ACTIVE_WINDOW"];
    let command = Command::new("xprop").args(args).output().unwrap();
    let active_window_id = String::from_utf8(command.stdout).unwrap();
    let window_id_re = Regex::new(r"(0x[0-9a-f]+)").unwrap();

    window_id_re
        .captures(active_window_id.as_str())
        .unwrap()
        .get(1)
        .unwrap()
        .as_str()
        .to_owned()
}
