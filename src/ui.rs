pub mod build;
pub mod callback;
pub mod constants;
pub mod state;
use super::Opts;
use crate::window_info::*;
use gtk::{EntryExt, WidgetExt};
use ngrammatic::Corpus;

pub fn init(opts: &Opts) {
    if gtk::init().is_err() {
        panic!("Failed to initialize GTK.");
    }

    build::init_window(opts);
}

pub fn show(window_infos: Vec<WindowInfo>, corpus: Corpus) {
    if window_infos.is_empty() {
        return;
    }

    state::set_items(window_infos);
    state::set_corpus(corpus);
    state::get_search_entry().set_text("");

    build::rebuild();

    state::get_window().show_all();

    gtk::main();
}
