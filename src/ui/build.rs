use super::callback;
use super::constants;
use super::state;
use super::Opts;
use crate::window_info::WindowInfo;
use gio::File;
use gtk::prelude::*;
use gtk::{
    Box as GtkBox, Button, ContainerExt, CssProvider, CssProviderExt, GtkWindowExt, Justification,
    Label, LabelExt, Orientation, PolicyType, ScrolledWindow, ScrolledWindowExt, SearchEntry,
    Widget, WidgetExt, Window, WindowType, STYLE_PROVIDER_PRIORITY_USER,
};
use pango::EllipsizeMode;

pub fn init_window(opts: &Opts) {
    let window = Window::new(WindowType::Toplevel);

    let css_provider = CssProvider::new();
    if let Some(css_file) = &opts.css_file {
        css_provider
            .load_from_file(&File::new_for_path(css_file))
            .unwrap();
    } else {
        css_provider
            .load_from_data(constants::CSS.as_bytes())
            .unwrap();
    }

    state::set_opts(opts.clone());
    state::set_style_provider(css_provider);
    state::set_window(window);

    let window = state::get_window();
    let style_context = window.get_style_context().unwrap();
    let style_provider = state::get_style_provider();
    style_context.add_provider(style_provider, STYLE_PROVIDER_PRIORITY_USER);

    window.set_title("Window Switcher");
    window.set_default_size(constants::WINDOW_DEFAULT_WIDTH, 0);
    window.set_decorated(false);

    window.connect_screen_changed(callback::on_screen_changed);
    window.connect_draw(callback::on_window_draw);
    window.connect_focus_out_event(callback::on_window_focus_out);
    window.connect_delete_event(callback::on_window_delete_event);
    window.connect_key_press_event(callback::on_window_input);

    build_window(&window);
}

pub fn rebuild() {
    let window = state::get_window();

    build_items();

    callback::on_screen_changed(&window, &None);
}

fn build_window(window: &Window) {
    let container = build_container();

    window.add(container);
}

fn build_container<'a>() -> &'a impl IsA<Widget> {
    let scrolled_window = ScrolledWindow::new(None, None);

    scrolled_window.set_min_content_height(constants::SCROLLED_WINDOW_DEFAULT_HEIGHT);

    let scrolled_window_policy = {
        if state::get_items().len() <= constants::ITEMS_COUNT_SCROLL_THRESHOLD {
            PolicyType::Never
        } else {
            PolicyType::Automatic
        }
    };
    scrolled_window.set_policy(PolicyType::Never, scrolled_window_policy);

    let style_context = scrolled_window.get_style_context().unwrap();
    let style_provider = state::get_style_provider();
    style_context.add_provider(style_provider, STYLE_PROVIDER_PRIORITY_USER);

    if let Some(scrolled_window_scrollbar) = scrolled_window.get_vscrollbar() {
        let style_context = scrolled_window_scrollbar.get_style_context().unwrap();
        let style_provider = state::get_style_provider();
        style_context.add_provider(style_provider, STYLE_PROVIDER_PRIORITY_USER);
    }

    let container = GtkBox::new(Orientation::Vertical, constants::VERTICAL_SPACING);

    let style_context = container.get_style_context().unwrap();
    let style_provider = state::get_style_provider();
    style_context.add_provider(style_provider, STYLE_PROVIDER_PRIORITY_USER);

    scrolled_window.add(&container);

    state::set_scrolled_window(scrolled_window);

    let style_context = container.get_style_context().unwrap();
    let style_provider = state::get_style_provider();
    style_context.add_provider(style_provider, STYLE_PROVIDER_PRIORITY_USER);

    let search_entry = build_search_entry();
    container.add(&search_entry);

    let items_container = GtkBox::new(Orientation::Vertical, constants::VERTICAL_SPACING);

    container.add(&items_container);

    search_entry.grab_focus();

    state::set_items_container(items_container);
    state::set_search_entry(search_entry);

    state::get_scrolled_window()
}

fn build_search_entry() -> SearchEntry {
    let search_entry = SearchEntry::new();

    let style_context = search_entry.get_style_context().unwrap();
    let style_provider = state::get_style_provider();
    style_context.add_provider(style_provider, STYLE_PROVIDER_PRIORITY_USER);

    search_entry.connect_search_changed(callback::on_search_entry_changed);
    search_entry.connect_focus_in_event(callback::on_search_entry_focus_in);
    search_entry.connect_key_press_event(callback::on_search_entry_input);

    search_entry
}

fn build_items() {
    let (items_container, items) = (state::get_items_container(), state::get_items());

    items_container
        .get_children()
        .iter()
        .for_each(|widget| items_container.remove(widget));

    items.iter().for_each(|item| {
        let widget = build_item(item);

        items_container.add(&widget);
    });

    if let Some(item) = items.first() {
        state::set_focused_item(Some(item.id().to_owned()));

        let style_context = items_container
            .get_children()
            .first()
            .unwrap()
            .get_style_context()
            .unwrap();
        style_context.add_class("focus");
    }
}

fn build_item(item: &WindowInfo) -> impl IsA<Widget> {
    let button = Button::new();

    let style_context = button.get_style_context().unwrap();
    let style_provider = state::get_style_provider();
    style_context.add_provider(style_provider, STYLE_PROVIDER_PRIORITY_USER);

    let label = Label::new(None);
    label.set_markup(format!("<small>{} ({})</small>", item.title(), item.command()).as_ref());
    label.set_justify(Justification::Left);
    label.set_ellipsize(EllipsizeMode::Middle);
    label.set_xalign(0.0);
    button.add(&label);

    button.connect_focus_out_event(callback::on_item_focus_out);
    button.connect_focus_in_event(callback::on_item_focus_in);
    button.connect_clicked(callback::on_item_clicked);
    button.connect_key_press_event(callback::on_item_input);

    WidgetExt::set_name(&button, item.id().as_ref());

    button
}
