use super::super::Opts;
use crate::window_info::WindowInfo;
use gtk::{Box as GtkBox, CssProvider, ScrolledWindow, SearchEntry, Window};
use ngrammatic::Corpus;

struct UIState {
    items: Vec<WindowInfo>,
    corpus: Option<ngrammatic::Corpus>,
    style_provider: Option<CssProvider>,
    items_container: Option<GtkBox>,
    focused_item: Option<String>,
    search_entry: Option<SearchEntry>,
    scrolled_window: Option<ScrolledWindow>,
    window: Option<Window>,
    rgba_available: bool,
    opts: Option<Opts>,
}

static mut UI_STATE: UIState = UIState {
    items: Vec::new(),
    corpus: None,
    style_provider: None,
    items_container: None,
    focused_item: None,
    search_entry: None,
    scrolled_window: None,
    window: None,
    rgba_available: true,
    opts: None,
};

pub fn set_items(items: Vec<WindowInfo>) {
    unsafe {
        UI_STATE.items = items;
    }
}

pub fn get_items<'a>() -> &'a Vec<WindowInfo> {
    unsafe { &UI_STATE.items }
}

pub fn get_items_mut<'a>() -> &'a mut Vec<WindowInfo> {
    unsafe { &mut UI_STATE.items }
}

pub fn set_corpus(corpus: Corpus) {
    unsafe {
        UI_STATE.corpus = Some(corpus);
    }
}

pub fn get_corpus<'a>() -> &'a Corpus {
    unsafe { UI_STATE.corpus.as_ref().unwrap() }
}

pub fn set_style_provider(style_provider: CssProvider) {
    unsafe {
        UI_STATE.style_provider = Some(style_provider);
    }
}

pub fn get_style_provider<'a>() -> &'a CssProvider {
    unsafe { UI_STATE.style_provider.as_ref().unwrap() }
}

pub fn set_window(window: Window) {
    unsafe {
        UI_STATE.window = Some(window);
    }
}

pub fn get_window<'a>() -> &'a Window {
    unsafe { UI_STATE.window.as_ref().unwrap() }
}

pub fn set_scrolled_window(scrolled_window: ScrolledWindow) {
    unsafe {
        UI_STATE.scrolled_window = Some(scrolled_window);
    }
}

pub fn get_scrolled_window<'a>() -> &'a ScrolledWindow {
    unsafe { UI_STATE.scrolled_window.as_ref().unwrap() }
}

pub fn set_items_container(items_container: GtkBox) {
    unsafe {
        UI_STATE.items_container = Some(items_container);
    }
}

pub fn get_items_container<'a>() -> &'a GtkBox {
    unsafe { UI_STATE.items_container.as_ref().unwrap() }
}

pub fn set_search_entry(search_entry: SearchEntry) {
    unsafe {
        UI_STATE.search_entry = Some(search_entry);
    }
}

pub fn get_search_entry<'a>() -> &'a SearchEntry {
    unsafe { UI_STATE.search_entry.as_ref().unwrap() }
}

pub fn set_focused_item(focused_item: Option<String>) {
    unsafe {
        UI_STATE.focused_item = focused_item;
    }
}

pub fn get_focused_item<'a>() -> &'a str {
    unsafe { UI_STATE.focused_item.as_ref().unwrap() }
}

pub fn set_rgba_available(available: bool) {
    unsafe {
        UI_STATE.rgba_available = available;
    }
}

pub fn get_rgba_available() -> bool {
    unsafe { UI_STATE.rgba_available }
}

pub fn set_opts(opts: Opts) {
    unsafe {
        UI_STATE.opts = Some(opts);
    }
}

pub fn get_opts<'a>() -> &'a Opts {
    unsafe { UI_STATE.opts.as_ref().unwrap() }
}
