use super::build;
use super::constants;
use super::state;
use crate::window_info;
use cairo::{Context, Operator};
use gdk::{Event, EventFocus, EventKey};
use gdk::{Screen, ScreenExt};
use gtk::prelude::*;
use gtk::{Button, EntryExt, MovementStep, ScrolledWindowExt, SearchEntry, WidgetExt, Window};

pub fn on_screen_changed(window: &Window, _: &Option<Screen>) {
    let screen = window.get_screen().unwrap();

    let visual = screen.get_rgba_visual();

    let visual = {
        if let Some(visual) = visual {
            visual
        } else {
            eprintln!("RGBA is not available.");

            state::set_rgba_available(false);

            screen.get_system_visual().unwrap()
        }
    };

    window.set_visual(&visual);
}

pub fn on_window_draw(_: &Window, cairo_context: &Context) -> Inhibit {
    if state::get_rgba_available() {
        if let Some((r, g, b, a)) = state::get_opts().background_color {
            cairo_context.set_source_rgba(r, g, b, a);
        } else {
            cairo_context.set_source_rgba(0.0, 0.0, 0.0, 0.0);
        }
    } else {
        if let Some((r, g, b, _)) = state::get_opts().background_color {
            cairo_context.set_source_rgb(r, g, b);
        } else {
            cairo_context.set_source_rgb(38.0, 50.0, 56.0);
        }
    }

    cairo_context.set_operator(Operator::Source);

    cairo_context.paint();

    Inhibit(false)
}

pub fn on_window_focus_out(_: &Window, _: &EventFocus) -> Inhibit {
    quit();

    Inhibit(false)
}

pub fn on_window_delete_event(_: &Window, _: &Event) -> Inhibit {
    quit();

    Inhibit(false)
}

pub fn on_window_input(_: &Window, event_key: &EventKey) -> Inhibit {
    let keyval = event_key.get_keyval();
    let search_entry_text = state::get_search_entry().get_text().unwrap();

    if keyval == constants::KEYVAL_TAB {
        if search_entry_text == "" {
            select_focused_item();
        }

        return Inhibit(true);
    }

    if keyval == constants::KEYVAL_SHIFT_TAB {
        return Inhibit(true);
    } else if keyval == constants::KEYVAL_ESCAPE {
        quit();
    } else if keyval == constants::KEYVAL_RETURN || keyval == constants::KEYVAL_RETURN_NUMPAD {
        select_focused_item();
    } else if keyval == constants::KEYVAL_ARROW_DOWN
        || keyval == constants::KEYVAL_ARROW_DOWN_NUMPAD
    {
        if let Some(focused_item) = state::get_window().get_focus() {
            let items_container = state::get_items_container();

            if let Some(last_item) = items_container.get_children().last() {
                if WidgetExt::get_name(&focused_item) == WidgetExt::get_name(last_item) {
                    state::get_search_entry().grab_focus();

                    return Inhibit(true);
                }
            }
        }
    }

    Inhibit(false)
}

pub fn on_search_entry_changed(search_entry: &SearchEntry) {
    window_info::sort_window_infos(
        search_entry.get_text().as_ref().unwrap(),
        state::get_items_mut(),
        state::get_corpus(),
    );

    redraw_window();
}

pub fn on_search_entry_focus_in(_: &SearchEntry, _: &EventFocus) -> Inhibit {
    let items_container = state::get_items_container();

    let style_context = items_container
        .get_children()
        .first()
        .unwrap()
        .get_style_context()
        .unwrap();
    style_context.add_class("focus");

    Inhibit(false)
}

pub fn on_search_entry_input(_: &SearchEntry, event_key: &EventKey) -> Inhibit {
    let keyval = event_key.get_keyval();

    if keyval == constants::KEYVAL_ARROW_DOWN || keyval == constants::KEYVAL_ARROW_DOWN_NUMPAD {
        let items_container = state::get_items_container();

        let style_context = items_container
            .get_children()
            .first()
            .unwrap()
            .get_style_context()
            .unwrap();
        style_context.remove_class("focus");

        if let Some(item) = items_container.get_children().iter().nth(1) {
            item.grab_focus();
        }

        return Inhibit(true);
    } else if keyval == constants::KEYVAL_ARROW_UP || keyval == constants::KEYVAL_ARROW_UP_NUMPAD {
        let items_container = state::get_items_container();

        let style_context = items_container
            .get_children()
            .first()
            .unwrap()
            .get_style_context()
            .unwrap();
        style_context.remove_class("focus");

        if let Some(item) = items_container.get_children().last() {
            item.grab_focus();
        }

        return Inhibit(true);
    }

    Inhibit(false)
}

pub fn on_item_focus_out(button: &Button, _: &EventFocus) -> Inhibit {
    let style_context = button.get_style_context().unwrap();
    style_context.remove_class("focus");

    Inhibit(false)
}

pub fn on_item_focus_in(button: &Button, _: &EventFocus) -> Inhibit {
    state::set_focused_item(WidgetExt::get_name(button));

    let style_context = button.get_style_context().unwrap();
    style_context.add_class("focus");

    Inhibit(false)
}

pub fn on_item_clicked(_: &Button) {
    select_focused_item();
}

pub fn on_item_input(_: &Button, event_key: &EventKey) -> Inhibit {
    let keyval = event_key.get_keyval();

    if keyval != constants::KEYVAL_ARROW_UP
        && keyval != constants::KEYVAL_ARROW_UP_NUMPAD
        && keyval != constants::KEYVAL_ARROW_DOWN
        && keyval != constants::KEYVAL_ARROW_DOWN_NUMPAD
        && keyval != constants::KEYVAL_PAGE_DOWN
        && keyval != constants::KEYVAL_END
        && keyval != constants::KEYVAL_PAGE_UP
        && keyval != constants::KEYVAL_HOME
    {
        let search_entry = state::get_search_entry();

        search_entry.grab_focus();
        search_entry.emit_move_cursor(MovementStep::BufferEnds, 1, false);

        let vadjustment = state::get_scrolled_window().get_vadjustment().unwrap();

        vadjustment.set_value(vadjustment.get_lower());
    }

    Inhibit(false)
}

fn redraw_window() {
    build::rebuild();

    state::get_window().show_all();
}

fn select_focused_item() {
    let focused_item = state::get_focused_item();

    window_info::switch_to_window_by_id(focused_item);

    quit();
}

fn quit() {
    gtk::main_quit();

    state::get_window().hide();
}
