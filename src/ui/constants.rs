pub const WINDOW_DEFAULT_WIDTH: i32 = 600;
pub const SCROLLED_WINDOW_DEFAULT_HEIGHT: i32 = 400;
pub const VERTICAL_SPACING: i32 = 0;
pub const KEYVAL_ESCAPE: u32 = 65307;
pub const KEYVAL_RETURN: u32 = 65293;
pub const KEYVAL_RETURN_NUMPAD: u32 = 65421;
pub const KEYVAL_TAB: u32 = 65289;
pub const KEYVAL_SHIFT_TAB: u32 = 65056;
pub const KEYVAL_ARROW_UP: u32 = 65362;
pub const KEYVAL_ARROW_UP_NUMPAD: u32 = 65431;
pub const KEYVAL_ARROW_DOWN: u32 = 65364;
pub const KEYVAL_ARROW_DOWN_NUMPAD: u32 = 65433;
pub const KEYVAL_PAGE_DOWN: u32 = 65366;
pub const KEYVAL_END: u32 = 65367;
pub const KEYVAL_PAGE_UP: u32 = 65365;
pub const KEYVAL_HOME: u32 = 65360;
pub const ITEMS_COUNT_SCROLL_THRESHOLD: usize = 15;

pub const CSS: &str = "
* {
  background: transparent;
  color: white;
  border-radius: 0;
  font-size: 1.25rem;
}

scrolledwindow {
  background: rgba(38.0, 50.0, 56.0, 0.85); /* #263238 */
  border-radius: 5px;
}

scrollbar, scrollbar slider {
  border-radius: 150px;
}

scrollbar {
  margin: 5px 0 5px 0;
}

scrollbar slider {
  background: #ff7042;
}

entry.search, button {
  background: transparent;
  border: none;
}

entry.search {
  padding: 7.5px 10px;
}

entry.search:focus {
  border: none;
}

button {
  padding: 10px;
}

button.focus {
  color: #ff7042;
}

button:focus, button:hover {
  outline: none;
}

button:hover {
  color: #ff7042;
}
";
