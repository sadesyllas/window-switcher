#![feature(process_exitcode_placeholder)]
#![feature(termination_trait_lib)]
#![feature(const_vec_new)]

pub mod os;
pub mod ui;
pub mod window_info;

#[derive(Clone)]
pub struct Opts {
    pub current_workspace_only: bool,
    pub css_file: Option<String>,
    pub background_color: Option<(f64, f64, f64, f64)>,
}
