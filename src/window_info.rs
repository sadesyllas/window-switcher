use crate::os;
use crate::Opts;
use ngrammatic::{Corpus, CorpusBuilder, Pad};
use regex::Regex;
use std::cmp::Ordering;
use std::collections::HashMap;

#[derive(Debug)]
pub struct WindowInfo {
    id: String,
    title: String,
    command: String,
}

impl WindowInfo {
    pub fn new(id: String, title: String, command: String) -> WindowInfo {
        WindowInfo { id, title, command }
    }

    pub fn id(&self) -> &str {
        self.id.as_str()
    }
    pub fn title(&self) -> &str {
        self.title.as_str()
    }

    pub fn command(&self) -> &str {
        self.command.as_str()
    }
}

pub fn get_window_infos_and_corpus(opts: &Opts) -> (Vec<WindowInfo>, ngrammatic::Corpus) {
    let window_infos = os::current::build_window_infos(opts);

    let mut corpus = CorpusBuilder::new()
        .arity(2)
        .pad_full(Pad::Auto)
        .case_insensitive()
        .finish();

    for window_info in &window_infos {
        build_corpus(&mut corpus, window_info);
    }

    (window_infos, corpus)
}

pub fn sort_window_infos(
    query: &str,
    window_infos: &mut Vec<WindowInfo>,
    corpus: &ngrammatic::Corpus,
) {
    let mut results = corpus.search(query, 0.0);

    results.sort_by(|a, b| a.similarity.partial_cmp(&b.similarity).unwrap().reverse());

    let mut window_info_index = HashMap::new();

    results.iter().enumerate().for_each(|(index, result)| {
        let id: &str = result
            .text
            .split_whitespace()
            .take(1)
            .collect::<Vec<_>>()
            .first()
            .unwrap();

        window_info_index.insert(id, index);
    });

    window_infos.sort_by(|a, b| {
        if !window_info_index.contains_key(a.id.as_str()) {
            return Ordering::Greater;
        }

        if !window_info_index.contains_key(b.id.as_str()) {
            return Ordering::Less;
        }

        window_info_index[a.id.as_str()]
            .partial_cmp(&window_info_index[b.id.as_str()])
            .unwrap()
    });
}

pub fn switch_to_window_by_id(id: &str) {
    os::current::switch_to_window_by_id(id);
}

fn build_corpus(corpus: &mut Corpus, WindowInfo { id, title, command }: &WindowInfo) {
    let mut window_info_description =
        String::with_capacity(id.len() + 1 + title.len() + 1 + command.len());

    let not_alpha_re = Regex::new(r"[^a-zA-Z0-9.-]+").unwrap();
    let whitespace_re = Regex::new(r"\s+").unwrap();

    let id = id.to_lowercase();
    let id = not_alpha_re.replace_all(id.as_str(), " ");
    let id = whitespace_re.replace_all(&id, " ");

    let title = title.to_lowercase();
    let title = not_alpha_re.replace_all(title.as_str(), " ");
    let title = whitespace_re.replace_all(&title, " ");

    let command = command.to_lowercase();
    let command = not_alpha_re.replace_all(command.as_str(), " ");
    let command = whitespace_re.replace_all(&command, " ");

    window_info_description.push_str(&id);
    window_info_description.push_str(" ");
    window_info_description.push_str(&title);
    window_info_description.push_str(" ");
    window_info_description.push_str(&command);

    let window_info_description = whitespace_re.replace_all(window_info_description.as_ref(), " ");

    corpus.add_text(&window_info_description);
}
