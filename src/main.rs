use clap::{App, Arg};
use std::fs;
use window_switcher::{ui, window_info::*, Opts};

fn main() {
    let arg_current_workspace = Arg::with_name("current workspace")
        .short("w")
        .long("current-workspace")
        .help("Restricts the window list to the current workspace.");

    let arg_css_file = Arg::with_name("css file")
        .short("c")
        .long("css-file")
        .takes_value(true)
        .value_name("FILE")
        .help("Chooses a CSS file to use for styling the application's window.");

    let arg_rgba = Arg::with_name("rgba")
        .short("b")
        .long("rgba")
        .takes_value(true)
        .value_name("RGBA")
        .help("Chooses the background for the application's window (e.g., 38.0,50.0,56.0,1.0).");

    let args = App::new("Window Switcher")
        .version("1.0.0")
        .arg(arg_current_workspace)
        .arg(arg_css_file)
        .arg(arg_rgba)
        .get_matches();

    if let Some(css_file) = args.value_of("css file") {
        if let Ok(css_file_metadata) = fs::metadata(css_file) {
            if !css_file_metadata.is_file() {
                panic!("Invalid CSS file path provided: {}.", css_file);
            }
        }
    }

    let rgba = if let Some(rgba) = args.value_of("rgba") {
        let rgba_parts: Vec<&str> = rgba.split(',').collect();

        if rgba_parts.len() != 4 {
            panic!("Invalid RGBA value provided: {}.", rgba);
        }

        let r: f64 = rgba_parts[0]
            .parse()
            .expect("Invalid value provided for the R component of the RGBA value.");
        let g: f64 = rgba_parts[1]
            .parse()
            .expect("Invalid value provided for the G component of the RGBA value.");
        let b: f64 = rgba_parts[2]
            .parse()
            .expect("Invalid value provided for the B component of the RGBA value.");
        let a: f64 = rgba_parts[3]
            .parse()
            .expect("Invalid value provided for the A component of the RGBA value.");

        Some((r, g, b, a))
    } else {
        None
    };

    let opts = Opts {
        current_workspace_only: args.is_present("current workspace"),
        css_file: args
            .value_of("css file")
            .map(|css_file| css_file.to_owned()),
        background_color: rgba,
    };

    ui::init(&opts);

    let (window_infos, corpus) = get_window_infos_and_corpus(&opts);

    ui::show(window_infos, corpus);
}
